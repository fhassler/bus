---
layout: post
title: Es ist schon wieder passiert
subtitle: OMG, es ist wieder passiert. Nach einem Zahnarzttermin in Linz wollte ich ins Büro nach Hagenberg fahren.
tags: [bus]

---

Jetzt ist es ja so, dass man in den Ferien vormittags eigentlich nicht nach Hagenberg oder von Hagenberg wegkommt. Die Busse fahren da einfach nicht. Ich vermute das liegt an der sommerlichen nicht-Auslastung. Die in der Eile rausgesuchte Verbindung war jedenfalls eine mit dem Zug (OEBB) und über St. Georgen an der Gusen. Tja, das bedeutet, dass die Strecke eine Zone länger ist und meine Monatskarte (Neumarkt im Muhlkreis<> Linz) nicht ausreicht. Der Zug macht eben eine große Schleife und man sieht viel von der Gegend dort!
Da ich wirklich knapp dran war, habe ich mir also am Linzer Hauptbahnhof eine Karte Linz > Pregarten gekauft, und darauf spekuliert, dass ich bei nächster Gelegenheit jemanden Fragen werde, der sich damit auskennt, welche Karte ich kaufen hätte müssen, damit meine Monatskarte angerechnet wird. Kurz vor St. Georgen ist dann eine Zugbegleiterin aufgetaucht, die mir das so erklärt hat:

> Sie: Da hätten Sie die Karte ab Linz Stadtgrenze kaufen müssen.<br>
> Ich: Aber wenn sie sich Oberösterreich als Karte vorstellen und die OÖVV-Zonen drüberlegen fehlt mir eigentlich nur eine Zone. Kann ich die irgendwie Upgraden?<br>
> Sie: (hektisch)... ich komm gleich wieder

Wir waren in eine Haltestelle eingefahren. Minuten später kam sie mit einem Kollegen wieder. Seine Idee war, dass das schon seine Richtigkeit hat, dass ich hier eine neue Karte gekauft habe da ein Upgrade nicht möglich ist. Am leichtesten tu ich mir, so seine Meinung, wenn ich die Monatskarte einfach mit der zusätzlichen Zone über St. Georgen an der Gusen kaufe. Das ist in meinen Augen eine Option wenn ich öfter mit dem Zug fahre, aber:

Die Verbindung war gut geplant mit einem Anschluss am Verkehrsknotenpunkt Pregarten. Der Zug hatte zwei Minuten verspätung und der Anschlusbus in Pregarten war fort. Dank spätsommerlicher Temperaturen wurde mir beim Spaziergang in den Hagenberger Softwarepark auch nicht kalt und ca 30 Minuten später kam ich ordentlich durchgeschwitzt im Büro an. Juhu.
