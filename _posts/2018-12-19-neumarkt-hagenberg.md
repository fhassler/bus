---
layout: post
title: Neumarkt > Hagenberg
subtitle: Frisch is es worn
tags: [bus]
---

Ich schreibe, weil es mir heute wieder einmal so kalt geworden ist, dass ich frustrierter Öffi-Fahrer mich genötigt sehe zu schreiben:

- Wieder einmal gibt es Probleme beim Umstieg in Götschka.
- Wieder einmal sehe ich das Problem nicht bei den Busfahrern die nur ihren Job machen.
- Wieder einmal frage ich mich, ob Sie als Planende nicht öfter mit mir mitfahren sollten.

 <img src="/img/2018-12-19-073559.jpg" alt="Screenshot der mobilen Fahrplanauskunft des OÖVV." class="img-inline img-left" style="max-width: 400px;float:left;">
Ich möchte Sie daran erinnern, dass es Menschen gibt, die die Querverbindung Neumarkt im Mühlkreis - Hagenberg nutzen. Ich vertraue ja manchmal auf die Technik, die behauptet, ich könne die Strecke tatsächlich in den versprochenen 12 Minuten öffentlich schaffen. 12 Minuten sind praktisch fast so lang, wie wenn ich selbst mit dem PKW fahren würde. Sehr positiv also.  Aber die Wahrheit sieht so anders aus, dass ich nur noch den Kopf schütteln kann:

Der Bus aus Linz ist ab 14:00 Uhr de Facto immer verspätet und es ist immer ein hinzittern (ich kenne das ja anders), ob der Anschluss abwartet. Dass ich bisher noch nie stehengelassen wurde liegt daran, dass mich ein Arbeitskollege immer wenn er mich in Götschka stehen sieht, mitnimmt. An dieser Stelle danke ich der Klimaerwärmung für den milden Herbst der mich wirklich lange mit dem Fahrrad fahren hat lassen. Laut eigener Statistik bin ich heuer um die 80 mal mit dem Fahrrad statt mit dem Bus gefahren.

Mein heutiges Problem betrifft jedoch die andere Richtung: Wie kann es sein, dass zwei(?) Jahre nach Umstieg auf das neue "Umstiegssystem Götschka" für die Strecke Neumarkt - Hagenberg, der Bus mit Abfahrt um 6:50 Uhr in Götschka gefühlte 100% der Tage unpünktlich abfährt?

Heute 7:11 Uhr Abfahrt in Götschka anstatt 6:50 Uhr.
Das sind 21 Minuten später!

Das schaut für mich nach Absicht oder Unwillen aus, hier etwas verändern zu wollen, immerhin hatten wir gerade die Fahrplanumstellung wo eine Änderung möglich gewesen wäre.
Gibt es von Ihrer Seite noch Bestrebungen etwas zu ändern, oder kann ich es als gegeben hinnehmen, dass es weiterhin ungewiss ist, wann ich morgens in der Arbeit sein werde?

