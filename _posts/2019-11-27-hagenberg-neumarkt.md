---
layout: post
title: Neumarkt > Hagenberg
subtitle: Vorweihnachtliche Eisgrüße
tags: [bus]
---
**Wir schreiben den 20. November 2019**. Ich habe mich wirklich schon lange
nicht mehr gemeldet. Das liegt daran, dass wir einen schönen, warmen
Herbst hatten. Wirklich genau richtig, um das Fahrrad noch nicht
einzuwintern. Alles Gute hat aber irgendwann ein Ende, so auch meine
Radsaison 2019. Es ist 18:19 Uhr während ich diese Zeile tippe und
langsam werden meine Finger steif von der Kälte. Ich stehe hier in
Götschka, wie so oft in den letzten Jahren. Wie konnte ich auch
annehmen, dass eine so einfache Verbindung, wie die von Hagenberg nach
Neumarkt, von der ich ja erst seit drei Jahren berichte, simple 8km,
heuer funktionieren. Auch die paar durchgehenden Busse, die hier
eingerichtet wurden bringen nichts, wenn man Termine hat. Der Beginn
meiner Bussaison wird leider stark von diesem heutigen Vorfall getrübt.
Ich spiele mit dem Gedanken statt der Monatskarte Spikes für mein
Fahrrad zu kaufen.

**Update, 27. November 2019**: Auch an diesem Mittwoch ergibt es sich so,
dass ich den durchgehenden Bus von Hagenberg über Neumarkt nach
Freistadt (Abfahrt in Hgb um 17:12 Uhr) aufgrund eines beruflichen
Termins versäume. Ich beschließe, noch einmal mein Glück zu versuchen.
Kurz nach halb sechs verlasse ich also das Büro und spaziere zur
Bushaltestelle um den Bus um 17:42 Uhr zu erreichen. Laut meiner
Wetter-App hat es in Hagenberg 2°C. Es ist finster aber die
Weihnachtsbeleuchtung ist bereits eingeschaltet. Dann die erste
Ernüchterung: Ich öffne via QR-Code am Fahrplanaushang die Seite mit den
Live-Zeiten. Mein Bus wird erst in Hagenberg sein, wenn mein Anschluss
in Götschka bereits weg ist. Als der Bus durch Anitzberg fährt sehe ich
in der ÖVV-Fahrplan-App, dass mein Anschluss gerade losgefahren ist.
Ankunft in Götschka um 18:02 Uhr.
Auch die Tatsache, dass mir dir OÖVV-Fahrplan-App mitteilt, dass ich
meinen Anschluss vorraussichtlich nicht erreichen werde bringt mir
leider nichts. Ich muss ja irgendwie von der Arbeit nach Hause kommen.
Es ist jedenfalls kalt und ich stehe schon wieder in Götschka. Damit
wünsche ich Ihnen frostige vorweihnachtliche Grüße.
