---
layout: post
title: Neumarkt > Hagenberg
subtitle: E-Mail an den Verkehrsverbund Oberösterreich
tags: [bus]
---
Jetzt ist schon wieder was passiert, würde Wolf Haasschreiben. Florian Haßler schreibt: Bei heftigem Schneetreiben und Temperaturen um den Gefrierpunkt bin ich heute von 6:50 Uhr bis 7:18 Uhr in Götschka Nord (die Metropole!!) gestanden und habe mich von den vorbeifahrenden PKWs mitSchneematsch bespritzen lassen. Die Wartehütte konnte ich leider nichtaufsuchen, da der Busfahrer dann vermutlich an mir vorbeigefahren wäre. Außerdem stinkts da drinnen nach kaltem Zigarettenrauch was mir auf nüchternen Magen nicht so gut tut. Ich hatte jedenfalls keinen Anschluss in Götschka Nordund bin mit dem nachfolgenden Bus zuerst nach Loibersdorf und dann mit dem Bus 316 nach Hagenberg gefahren.

Für die Strecke Neumarkt im Mühlkreis (Abfahrt um 6:44Uhr) nach Hagenberg im Mühlkreis (Ankunft 7:35 Uhr) hab ich anstatt derversprochenen 12 Minuten also marginal länger gebraucht und fühle mich stark erkältet.

Frostige Grüße,

Florian Haßler
