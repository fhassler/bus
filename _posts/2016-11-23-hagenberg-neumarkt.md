---
layout: post
title: Hagenberg > Neumarkt
subtitle: E-Mail an den Verkehrsverbund Oberösterreich
tags: [bus]
---
Sehr geehrte Damen und Herren,

mir ist kalt. ich bin eine Stunde in der "Weltmetropole" Götschka beim ehemaligen GH Lamplmayr gestanden. Waren Sie bereits einmal dort? Zwischen 17:00 und 19:00 Uhr kann man dem Bauern dabei zuschauen, wie er die Kühe in den Melkstand treibt. Unerfreulicherweise war das seit vergangenem Dienstag bereits das zweite Mal, dass ich bei den augenblicklichen Außentemperaturen (-4°C und sinkend) stehengelassen wurde.
Wenn mich, wie heute, der eine Busfahrer aussteigen lässt, und der andere gar nicht auf mich wartet, brauche ich, für eine 8,5km kurze Strecke plötzlich, wie bereits vorige Woche, mehr als 60 Minuten. Das ist inakzeptabel!

Ich kann im Scotty nachschauen, wie weit die Busse bereits sind. Sie haben ja bereits sehr viele Ihrer Busse mit GPS-Empfängern und Sendern ausgestattet. Mir ist aufgefallen, dass einige Busfahrer auf ihren neuen Touch-Displays zusätzlich zu den kommenden Haltestellen auch die Anschlussbusse mit den Verspätungen (ähnlich dem, was ich im Scotty sehe) sehen können. Es gibt auch bereits Busse, zwischen Linz und Neumarkt, die mit einem Bildschirm ausgestattet sind, wo die Fahrgäste die nächste Haltestelle und Anschlussmöglichkeiten angezeigt kommen. Warum werde ich nun stehengelassen?

Nachdem das bereits das zweite Mal ist, dass ich hier in Götschka stehe und friere, sende ich dieses E-Mail auch an "Den Softwarepark". Die Damen und Herren von dort wollten in einer Mailaussendung Ende Juli wissen, wer von der Fahrplanumstellung betroffen ist. Ich habe mich gemeldet und seitdem nichts mehr gehört. Warum eigentlich nicht?
Wenn ich über eine Stunde für die 8km von Neumarkt nach Hagenberg brauche, kann ich bereits mit dem Bus nach Linz pendeln. Die Firma KEBA, würde sich zB als Alternative anbieten. Dort komme ich schneller hin und muss nicht umsteigen und stundenlang in der Kälte stehen. Ergo: Der Standort Hagenberg hat für mich durch die Umstellung des Busplans mit 1. August 2016 stark an Attraktivität verloren.

Ich freue mich bereits über Ihre zahlreichen Antworten und verbleibe,

mit freundlichen Grüßen aus der warmen Badewanne,

Florian Haßler

## Anhänge
![Bus 399](/img/2016-11-23-hagenberg-neumarkt/2016-11-23-oebb1.JPG){:class="img-responsive"}
![Anschlussbus](/img/2016-11-23-hagenberg-neumarkt/2016-11-23-oebb2.JPG){:class="img-responsive"}
