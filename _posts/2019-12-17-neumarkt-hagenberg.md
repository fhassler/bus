---
layout: post
title: Neumkarkt > Hagenberg
subtitle: E-Mail an den Verkehrsverbund Oberösterreich
tags: [bus]
---

Ich mag frische Luft, sehr sogar. Heute Morgen hatte ich endlich wieder die Gelegenheit, einem spektakulären Sonnenaufgang mit wahnsinnigem Morgenrot und Nebel in Götschka (der Weltmetropole) beizuwohnen. Ein Wahnsinn. Wirklich. So ein intensives rot sieht man eigentlich nur genau dann, wenn die Erde in dem Winkel zur Sonne steht, wie sie es um diese Jahreszeit zu tun pflegt.
Gehe ich Recht in der Annahme, dass die Busfahrer eine Dienstanweisung erhalten haben, dass sie alle Fahrkarten kontrollieren müssen und jeder Fahrgast auch eine Fahrkarte haben muss. Da mein heutiger Busfahrer in einer unglaublich charmanten Art alle Fahrgäste (in diesem Fall Schüler*innen) ohne Ausweis genau das spüren hat lassen, hatte er 3 Minuten Verspätung und unser Anschluss hat diese Verspätung nicht abgewartet. 

Jetzt möchte ich nicht mehr mit mehr Details sparen. Hier meine gewünschte Verbindung:
> 312 ab 6:44 in Neumarkt im Mühlkreis - an 6:48 Uhr in Götschka Nord
> 311 ab 6:50 Götschka Nord - an 6:56 Uhr in Hagenberg

Die Verkehrsverbund-Fahrplan-App hat ordnungsgemäß vermeldet:

> Der Anschluss kann voraussichtlich nicht erreicht werden.

Meine tatsächlich Ankunftszeit in Hagenberg war dann 7:47 Uhr, da auch der nachfolgende 311er 6 Minunten Verspätung hatte, also doch 6:44 - 7:47 = ~1 Stunde für 8km asphaltierte Straße.

Frau Falkner hat mir in Ihrem E-Mail vom 18.2.2019 versichert:
> Der Umstieg in Götschka wird nicht aufgelöst, aber wir haben die Anschlusssicherung in Götschka Nord mit bis zu 10 Minuten bemessen und die Wartepflicht wurde vertraglich vereinbart.
Umsetzungszeitpunkt: 17.2.2019

Mit mir an der Bushaltestelle waren 4 Schülerinnen, 1 älterer Herr und 1 Arbeitskollege = 7 Personen, die am 17.12. 2019 in der Kälte stehen gelassen wurden obwohl Sie eine Vereinbarung mit dem Busunternehmen haben?

Nachdem ich frische Luft mag und laufenderweise auch ca. eine Stunde brauche, werde ich heute Abend die herbstlichen Temperaturen ausnutzen und wieder nach Neumarkt laufen. Dann bin ich nicht von Vereinbarungen abhängig, die nicht eingehalten werden und grantige Busfahrer muss ich mir auch nicht anhören.

