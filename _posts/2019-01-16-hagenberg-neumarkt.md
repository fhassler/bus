---
layout: post
title: Neumarkt > Hagenberg
subtitle: Gestrandet in Götschka
tags: [bus]
---


Wow, haben Sie heute aus dem Fenster gesehen. Nach Wochen (zumindest gefühlt) voller Nebel war der Himmel blau. Vereinzelt konnte man unter Umständen leichte Quellwolken am Himmel ausmachen - dennoch - Sonne, Schnee, leichtes Tauwetter. Mag ich sehr.

<img src="/img/2019-01-16-hagenberg-neumarkt/20190116_170641.jpg" alt="Foto von Neumarkt Richtung Götschka, Unterweitersdorf, Linz" class="img-inline img-left" style="max-width: 400px;float:left;">

Gestatten Sie, dass ich etwas Unschönes berichte? Ich bin gestrandet. In Götschka. Und meine Tochter (8 Jahre) hat mich zu Hause erwartet. Alleine. Und ich bin nicht gekommen. Eines der Worst-Case-Szenarios aus meiner Kindheit war, dass ich zu Hause bin und meine Eltern plötzlich nicht mehr auftauchen. Was sollte ich dann machen? Können Sie sich ungefähr hinenfühlen? Achja, meine Tochter hat mich einfach angerufen und sich erkundigt, warum ich nicht wie vereinbart um kurz nach vier zu Hause war.

Diese Technik, nicht? Meine Tochter hat unser Festnetztelefon benutzt und ich mein Smartphone. Mein Smartphone, ein Stück Technik, die mir sogar im unwegsamsten Gelände ermöglicht, meinen Standort festzustellen oder im Idealfall Hilfe zu holen. Die Technik machts möglich, dass wir uns beim Diskonter um (viel zu) wenig Geld ein Smartphone besorgen, dass uns via Google-Maps auf ein paar Meter genau mitteilt, wo wir uns gerade befinden. Mit ein paar Apps ([zum Beispiel dieser hier](https://f-droid.org/en/packages/net.fabiszewski.ulogger/ "externer Link zum Android Package von uLoger")) kann ich sogar in regelmäßigen Abständen meinen Standort an einen Server schicken lassen, sogar ohne Google. Schon ziemlich cool und gleichzeit sehr traurig, dass ich als Privatperson das kann und eine [GmbH im alleinigen Eigentum des Landes Oberösterreich](https://www.ooevv.at/ "externer Link zur Webseite des oberösterreichischen Verkehrsverbund") kriegts nicht auf die Reihe und lässt auch 2019 wieder eine Handvoll Schüler und zweite Handvoll arbeitende Bevölkerung in Götschka, bei der Milchtankstelle, stehen. So persönlich erlebt am 16. Jänner 2019 mit verspäteter Abfahrt in Hagenberg und Strandung in Götschka.

> Verbindung laut Busplan:<br>
> Bus 311, ab 15:45 Uhr in Hagenberg im Mühlkreis Ortsmitte, an 15:51 Uhr in Götschka Nord.<br>
> Bus 312, ab 15:54 Uhr in Götschka Nord, an 15:59 Uhr in Neumarkt im Mühlkreis Ortsmitte.
