---
layout: post
title: Hagenberg > Neumarkt
subtitle: E-Mail an den Verkehrsverbund Oberösterreich
tags: [bus]
---
Sehr geehrte Damen und Herren!

Vielen Dank für Ihre Antworten und Anrufe.

Ich wurde heute (9. Jänner 2017) um 17:10 Uhr freundlicherweise vom Altbauern (Peppi Immerle) aus Götschka mit dem PKW nach Hause (Neumarkt, Linzer Straße 28) gebracht. Vermutlich würde er sich freuen, wenn ihm der Busfahrer, der den Anschluss nicht abwarten konnte, eine Tafel Schokolade vorbeibringen würde :-)

Vielen Dank weiterhin führ Ihre Bemühungen in dieser Sache.

Mit freundlichen Grüßen,

Florian Haßler
