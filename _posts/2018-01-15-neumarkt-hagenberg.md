---
layout: post
title: Neumarkt > Hagenberg
subtitle:
tags: [bus]
---

Wollte wie immer um 6:44 Uhr in Neumarkt losfahren und in Hagenberg ankommen. Der erste Bus in Hagenberg war pünktlich wie immer (fährt ja auch in Neumarkt los). Beim Umsteigen in Götschka hat es mich dann allerdings erwischt:

- kein Bus an der Haltestelle
- kalt
- bibber
- Frost
- ooohhhh, ein Bus

Der Busfahrer hat sich entschuldigt. Auf der Fahrt nach Linz, die er vorher fahren durfte hat ihm jemand direkt hinter dem Fahrersitz in den Bus gekotzt. Dementsprechend kalt und muffig wars im Bus und ich war froh, um 7:10 Uhr in Hagenberg angekommen zu sein.
