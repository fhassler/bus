---
layout: page
title: Impressum / Datenschutz
subtitle: Was Sie wissen sollten
---

## Impressum

Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz:

Florian Haßler<br>
Linzer Straße 28<br>
4212 Neumarkt im Mühlkreis<br>
Österreich

**Tel.:** +43 7941 710 81<br>
**E-Mail:** mail@florian-hassler.net

*Quelle: Erstellt mit dem Impressum Generator von firmenwebseiten.at in Kooperation mit aestomed.at*

## EU-Streitschlichtung

Gemäß Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-Verordnung) möchten wir Sie über die Online-Streitbeilegungsplattform (OS-Plattform) informieren.
Verbraucher haben die Möglichkeit, Beschwerden an die Online Streitbeilegungsplattform der Europäischen Kommission unter [http://ec.europa.eu/odr?tid=221075792](http://ec.europa.eu/odr?tid=221075792) zu richten. Die dafür notwendigen Kontaktdaten finden Sie oberhalb in unserem Impressum.

Wir möchten Sie jedoch darauf hinweisen, dass wir nicht bereit oder verpflichtet sind, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.


## Haftung für Inhalte dieser Webseite

Wir entwickeln die Inhalte dieser Webseite ständig weiter und bemühen uns korrekte und aktuelle Informationen bereitzustellen. Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Webseite übernehmen, speziell für jene die seitens Dritter bereitgestellt wurden.

Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitte wir Sie uns umgehend zu kontaktieren, Sie finden die Kontaktdaten im Impressum.


## Haftung für Links auf dieser Webseite

Unsere Webseite enthält Links zu anderen Webseiten für deren Inhalt wir nicht verantwortlich sind. Haftung für verlinkte Websites besteht laut § 17 ECG für uns nicht, da wir keine Kenntnis rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und wir Links sofort entfernen würden, wenn uns Rechtswidrigkeiten bekannt werden.

Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitte wir Sie uns zu kontaktieren, Sie finden die Kontaktdaten im Impressum.


## Urheberrechtshinweis

Alle Inhalte dieser Webseite (Bilder, Fotos, Texte, Videos) unterliegen dem Urheberrecht. Falls notwendig, werden wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.


## Bildernachweis

Die Bilder, Fotos und Grafiken auf dieser Webseite sind urheberrechtlich geschützt.

Die Bilderrechte liegen bei Florian Haßler


# Datenschutzerklärung
## Datenschutz

Wir haben diese Datenschutzerklärung (Fassung 14.10.2018-221075792) verfasst, um Ihnen gemäß der Vorgaben der Datenschutz-Grundverordnung (EU) 2016/679 und dem Datenschutzgesetz (DSG) zu erklären, welche Informationen wir sammeln, wie wir Daten verwenden und welche Entscheidungsmöglichkeiten Sie als Besucher dieser Webseite haben.

Leider liegt es in der Natur der Sache, dass diese Erklärungen sehr technisch klingen, wir haben uns bei der Erstellung jedoch bemüht die wichtigsten Dinge so einfach und klar wie möglich zu beschreiben.
Automatische Datenspeicherung

Wenn Sie heutzutage Webseiten besuchen, werden gewisse Informationen automatisch erstellt und gespeichert, so auch auf dieser Webseite.

Wenn Sie unsere Webseite so wie jetzt gerade besuchen, speichert unser Webserver (Computer auf dem diese Webseite gespeichert ist) automatisch Daten wie

* die Adresse (URL) der aufgerufenen Webseite
* Browser und Browserversion
* das verwendete Betriebssystem
* die Adresse (URL) der zuvor besuchten Seite (Referrer URL)
* den Hostname und die IP-Adresse des Geräts von welchem aus zugegriffen wird
* Datum und Uhrzeit

in Dateien (Webserver-Logfiles).

In der Regel werden Webserver-Logfiles zwei Wochen gespeichert und danach automatisch gelöscht. Wir geben diese Daten nicht weiter, können jedoch nicht ausschließen, dass diese Daten beim Vorliegen von rechtswidrigem Verhalten eingesehen werden.
Die Rechtsgrundlage besteht nach Artikel 6  Absatz 1 f DSGVO (Rechtmäßigkeit der Verarbeitung) darin, dass berechtigtes Interesse daran besteht, den fehlerfreien Betrieb dieser Webseite durch das Erfassen von Webserver-Logfiles zu ermöglichen.


## Rechte laut Datenschutzgrundverordnung

Ihnen stehen laut den Bestimmungen der DSGVO und des österreichischen Datenschutzgesetzes (DSG) grundsätzlich die folgende Rechte zu:

* Recht auf Berichtung (Artikel 16 DSGVO)
* Recht auf Löschung („Recht auf Vergessenwerden“) (Artikel 17 DSGVO)
* Recht auf Einschränkung der Verarbeitung (Artikel 18 DSGVO)
* Recht auf Benachrichtigung – Mitteilungspflicht im Zusammenhang mit der Berichtigung oder Löschung personenbezogener Daten oder der Einschränkung der Verarbeitung (Artikel 19 DSGVO)
* Recht auf Datenübertragbarkeit (Artikel 20 DSGVO)
* Widerspruchsrecht (Artikel 21 DSGVO)
* Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung — einschließlich Profiling — beruhenden Entscheidung unterworfen zu werden (Artikel 22 DSGVO)

Wenn Sie glauben, dass die Verarbeitung Ihrer Daten gegen das Datenschutzrecht verstößt oder Ihre datenschutzrechtlichen Ansprüche sonst in einer Weise verletzt worden sind, können Sie sich bei der Aufsichtsbehörde beschweren, welche in Österreich die Datenschutzbehörde ist, deren Webseite Sie unter [https://www.dsb.gv.at/](https://www.dsb.gv.at/) finden.

*Quelle: Erstellt mit dem Datenschutz Generator von firmenwebseiten.at in Kooperation mit trocado.at*
