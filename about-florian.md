---
layout: page
title: Über Florian
subtitle: Warum er gerne mit dem Bus fährt 
---

## Busfahren

- ist billig
- macht Spaß
- lässt ihn einfach mal aus dem Fenster schauen
- gibt ihm die Zeit in Ruhe E-Mails zu beantworten
- kann er auch angeduselt machen


## Diese Seite

- Das Logo besteht aus zwei verschiedenen [OpenCliparts](https://openclipart.org), dem [gelben Autobus](https://openclipart.org/detail/305317/autobus-yellow) und einer [Männer-Silhouette](https://openclipart.org/detail/35263/man-silhouette).
- Das Template für Jekyll stammt von [Dean Attali](https://deanattali.com/beautiful-jekyll/), die Änderungen daran stammen zu 100% von Florian.

